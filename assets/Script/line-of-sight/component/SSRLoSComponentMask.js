//
const ssr = require('../namespace/SSRLoSNamespace');

ssr.LoS.Component.Mask = cc.Class({
    extends: cc.Component,
    editor: CC_EDITOR && {
        menu: 'ssr/LoS/Mask',
    },
    properties: {
        targets: {
            default         : [],
            type            : [cc.Node]
        }
    },
    onLoad:function() {
    },
    start:function() {
        if (!this.mask) {
            this.mask = this.node.getComponent("cc.Mask");
        }
        if (!this.mask) {
            cc.error("Component cc.Mask is needed for rendering!");
            return;
        }
        //
        // this.mask.inverted = true;
        this._losMaskNode = new ssr.LoS.Mask(this.mask);

        for (var i = 0; i < this.targets.length; i ++) {
            var target = this.targets[i];
            if (target.getComponent("SSRLoSComponentCore")) {
                this._losMaskNode.addTarget(target);
            }
            else {
                cc.log("No LoSCore component found on the target!");
            }
        }
    },
    updateSightNode:function(isForceLoSUpdate) {
        if (!this.mask || !this.node.active) {
            return;
        }
        if (this._losMaskNode.isNeedUpdate() || isForceLoSUpdate) {
            this._losMaskNode.updateTargets();
        }
    }
});

