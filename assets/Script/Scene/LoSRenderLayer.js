//
const ssr = require('../line-of-sight/namespace/SSRLoSNamespace');

cc.Class({
    extends: cc.Component,

    properties: {
    	robot: {
    		default: null,
    		type: cc.Node
    	}
    },

    start: function () {
        this.robotLoSComponent = this.robot.getComponent("SSRLoSComponentCore");
        this.robotLoSCore = this.robotLoSComponent.getLoSCore();
        this._initLoSComponentRender();
    },
    //
    _initLoSComponentRender:function() {
        var loSRenderGroup = this.node.getChildByName("LoSRenderGroup");
        // ray
        this._losComponentRenderRay = loSRenderGroup.getChildByName("RayRender").getComponent("SSRLoSComponentRender");
        // hitPoint
        this._losComponentRenderHitPoint = loSRenderGroup.getChildByName("HitPointRender").getComponent("SSRLoSComponentRender");
        // potentialBlockingEdge
        this._losComponentRenderPotentialBlockingEdge = loSRenderGroup.getChildByName("PotentialBlockingEdgeRender").getComponent("SSRLoSComponentRender");
        // blockingEdge
        this._losComponentRenderBlockingEdge = loSRenderGroup.getChildByName("BlockingEdgeRender").getComponent("SSRLoSComponentRender");
        // visibleEdge
        this._losComponentRenderVisibleEdge = loSRenderGroup.getChildByName("VisibleEdgeRender").getComponent("SSRLoSComponentRender");
        // sightVert
        this._losComponentRenderSightVert = loSRenderGroup.getChildByName("SightVertRender").getComponent("SSRLoSComponentRender");
        // sightRange
        this._losComponentRenderSightRange = loSRenderGroup.getChildByName("SightRangeRender").getComponent("SSRLoSComponentRender");
        // sightArea
        this._losComponentRenderSightArea = loSRenderGroup.getChildByName("SightAreaRender").getComponent("SSRLoSComponentRender");
        // sightLight
        this._losComponentRenderSightLight = loSRenderGroup.getChildByName("SightLightRender").getComponent("SSRLoSComponentRender");
        // mask
        this._losMaskNode = loSRenderGroup.getChildByName("MaskRender").getComponent("SSRLoSComponentMask");
    },
    updateRender:function(isForceLoSUpdate) {
        this._losComponentRenderSightArea.plot();
        this._losComponentRenderSightRange.plot();
        this._losComponentRenderRay.plot();
        this._losComponentRenderHitPoint.plot();
        this._losComponentRenderSightVert.plot();
        this._losComponentRenderPotentialBlockingEdge.plot();
        this._losComponentRenderBlockingEdge.plot();
        this._losComponentRenderVisibleEdge.plot();
        this._losComponentRenderSightLight.plot();
        this._losMaskNode.updateSightNode(isForceLoSUpdate);
    }
});
